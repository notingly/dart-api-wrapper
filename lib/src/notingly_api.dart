part of notingly_api;

class NotinglyApi {
  final Future<String> Function() getJWT;
  final Future<String> Function() getJWTRefresh;
  final Future<String> Function() getAPIUrl;
  final Future<void> Function(String) setJWT;
  final Future<void> Function(String) setJWTRefresh;
  final bool enableLoggingInConsole;
  final logger = Logger('NotinglyApi');
  Level loggingLevel;
  late Api rawApiConnector;
  final Dio httpClient = new Dio();

  NotinglyApi({
    required this.getJWT,
    required this.getJWTRefresh,
    required this.getAPIUrl,
    required this.setJWT,
    required this.setJWTRefresh,
    required this.enableLoggingInConsole,
    this.loggingLevel = Level.FINE,
  }) {
    if (enableLoggingInConsole) {
      Logger.root.level = loggingLevel;
      Logger.root.onRecord.listen((record) {
        print('${record.level.name}: ${record.time}: ${record.message}');
      });
    }

    this.rawApiConnector = new Api(
        getAPIUrl: this.getAPIUrl, getJWT: this.getJWT, httpClient: httpClient);
  }

  Future<User> login(
      {required String username, required String password}) async {
    logger.info('Logging in as $username');
    Map<String, dynamic> response =
        await rawApiConnector.authenticate(username, password);
    // create secure storage object and save token and refresh token
    await this.setJWT(response["token"]);
    await this.setJWTRefresh(response["jwt_refresh_token"]);
    logger.info('Logged in as $username successfully');
    return await getSelfUser();
  }

  Future<User> authenticateWithJWT() async {
    logger.info('Authenticating with jwt');
    try {
      return await getSelfUser();
    } catch (e) {
      if (e is JWTExpired) {
        Map<String, dynamic> refreshResponse =
            await rawApiConnector.refreshJWTToken(await this.getJWTRefresh());
        await this.setJWT(refreshResponse["token"]);
        await this.setJWTRefresh(refreshResponse["jwt_refresh_token"]);
        logger.info('JWT expired, new token obtained');
        return await getSelfUser();
      }
      throw AuthenticationFailedException();
    }
  }

/*
  USERS
  getUserById
  getSelfUser
  removeUser
  createUser
   */
  Future<User> getUser({required Id id}) async {
    // get user by id
    logger.info('Fetch user with the id ${id.value}');
    Map<String, dynamic> response = await rawApiConnector.getUser(id.value);

    logger.info('Fetching user with the id ${id.value} successfully');

    logger.finer('Data: ${response['response']}');
    return new User.fromJson(response['response'][0]);
  }

  Future<User> getSelfUser() async {
    // get information of logged in user
    logger.info('Fetch already logged in user');
    Map<String, dynamic> response = (await rawApiConnector.getSelfUser());
    logger.info('Fetch already logged in user successfully');

    logger.finer('Data: ${response['response']}');
    return User.fromJson(response['response'][0]);
  }

  Future<bool> removeUser({required Id id}) async {
    logger.info('Remove user with the id ${id.value}');
    Map<String, dynamic> response = await rawApiConnector.deleteUser(id.value);
    logger.info('Removing user with the id ${id.value} successfully');

    logger.finer('Data: ${response['response']}');
    return true;
  }

  Future<User> createUser(
      {required String username,
      required String password,
      String? code,
      List<String>? roles}) async {
    logger.info(
        'Create user with the name $username, code: ${code ?? "not set"}, roles: ${roles ?? "not set"}');
    Map<String, dynamic> response = await rawApiConnector
        .createUser(username, password, code: code, roles: roles);

    logger.finer('Data: ${response['response']}');
    return User.fromJson(response['response'][0]);
  }

  Future<User> updateUser(
      {required Id id,
      String? password,
      String? displayName,
      List<int>? avatarBytes}) async {
    logger.info('update user');
    Map<String, dynamic> response =
        await rawApiConnector.updateUser(id.value, password, displayName, avatarBytes);

    logger.finer('Data: ${response['response']}');
    return User.fromJson(response['response'][0]);
  }

/*
  GROUPS
  getGroups
  getGroupById
  createGroup
  modifyGroup
   removeGroup
   */
  Future<List<Group>> getGroups() async {
    logger.info('Fetch all groups the currently logged in user has access to');
    Map<String, dynamic> response = await rawApiConnector.getGroups();

    List<Group> groupList = <Group>[];
    for (Map<String, dynamic> elem in response['response']) {
      groupList.add(Group.fromJson(elem));
    }
    logger.info(
        'Fetched all groups the currently logged in user has access to successfully! Count: ${groupList.length}');
    logger.fine('Groups: ${groupList.map((g) => g.id.value).join(", ")}');
    logger.finer('Data: ${response['response']}');
    return groupList;
  }

  Future<Group> getGroup({required Id id}) async {
    logger.info('Fetch group with the ${id.value}');
    Map<String, dynamic> response =
        await rawApiConnector.getGroup(id.value);

    logger.info('Fetched group with the ${id.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return Group.fromJson(response['response'][0]);
  }

  Future<Group> createGroup(
      {required String name,
      required String description,
      required ApiColor color,
      GroupType type = GroupType.SCHOOL_CLASS}) async {
    logger.info('Create group');
    Map<String, dynamic> response = await rawApiConnector.postGroup(
        name, type.string, description, color.toObject());

    logger.info('Created group successfully');
    logger.fine(
        'Name: ${name}, type: ${type.string}, data: ${description}, color: ${color.toString()}');
    logger.finer('Data: ${response['response']}');
    return Group.fromJson(response['response'][0]);
  }

  Future<Group> updateGroup(
      {required Id id,
      String? name,
      String? description,
      ApiColor? color}) async {
    logger.info('Update group ${id.value}');
    Map<String, dynamic> response = await rawApiConnector.patchGroup(
        id.value, name, description, color?.toObject());

    logger.info('Updated group ${id.value} successfully');
    logger.fine('Name: ${name}, data: ${description}, color: ${color.toString()}');
    logger.finer('Data: ${response['response']}');
    return Group.fromJson(response['response'][0]);
  }

  Future<GroupMember> getSelfGroupMember({required Id groupId}) async {
    logger.info('Getting member self from group ${groupId.value}');

    Map<String, dynamic> response =
        await rawApiConnector.getGroupMember(groupId.value, 'self');

    logger.info('Got member self from group ${groupId.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return GroupMember.fromJson(response['response'][0]);
  }

  Future<GroupMember> getGroupMember(
      {required Id groupId, required Id userId}) async {
    logger.info('Getting member ${userId.value} from group ${groupId.value}');
    Map<String, dynamic> response =
        await rawApiConnector.getGroupMember(groupId.value, userId.value);

    logger.info(
        'Got member ${userId.value} from group ${groupId.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return GroupMember.fromJson(response['response'][0]);
  }

  Future<List<GroupMember>> getGroupMembers({required Id groupId}) async {
    logger.info('Getting members from group ${groupId.value}');
    Map<String, dynamic> response =
        await rawApiConnector.getGroupMembers(groupId.value);

    logger.info('Got members from group ${groupId.value} successfully!');
    logger.finer('Data: ${response['response']}');
    List<GroupMember> groupMemberList = <GroupMember>[];
    for (Map<String, dynamic> elem in response['response']) {
      groupMemberList.add(GroupMember.fromJson(elem));
    }
    return groupMemberList;
  }

  Future<bool> addGroupMember({required Id groupId, required Id userId}) async {
    logger.info('Add user ${userId.value} to group ${groupId.value}');
    if (userId.empty) throw NotAllInformationProvidedInClass();
    Map<String, dynamic> response =
        await rawApiConnector.addGroupMember(groupId.value, userId.value);

    logger.info(
        'Added user ${userId.value} to group ${groupId.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return true;
  }

  Future<bool> updateGroupMember(
      {required Id groupId,
      required Id memberId,
      required GroupPermissions permissions}) async {
    logger.info('Update member ${memberId.value} from group ${groupId.value}');
    Map<String, dynamic> response = await rawApiConnector.updateGroupMember(
        groupId.value, memberId.value, permissions.toInt());

    logger.info(
        'Updated member ${memberId.value} from group ${groupId.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return true;
  }

  Future<bool> removeGroupMember(
      {required Id groupId, required Id userId}) async {
    logger.info('Remove user ${userId.value} from group ${groupId.value}');
    Map<String, dynamic> response =
        await rawApiConnector.removeGroupMember(groupId.value, userId.value);

    logger.info(
        'Removed user ${userId.value} from group ${groupId.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return true;
  }

  Future<bool> removeGroup({required Id id}) async {
    logger.info('Remove group ${id.value}');
    Map<String, dynamic> response = await rawApiConnector.deleteGroup(id.value);

    logger.info('Removed group ${id.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return true;
  }

/*
  INVITES
  getInviteById
  createInvite
  useInvite
*/

  Future<Invite> getInviteById({required Id id}) async {
    logger.info('Fetch invite ${id.value}');
    Map<String, dynamic> response =
        await rawApiConnector.getInvite(id.value);

    logger.info('Fetched invite ${id.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return Invite.fromJson(response['response'][0]);
  }

  Future<Invite> createInvite({required Id groupId}) async {
    logger.info('Create invite');
    Map<String, dynamic> response =
        await rawApiConnector.postInvite(groupId.value);

    logger.info('Created invite successfully!');
    logger.finer('Data: ${response['response']}');
    return Invite.fromJson(response['response'][0]);
  }

  Future<Group> useInviteLink({required String link}) async {
    logger.info('Use invite ${link}');
    Map<String, dynamic> response =
        await rawApiConnector.postInviteJoin(link);

    logger.info('Used invite ${link} successfully!');
    logger.finer('Data: ${response['response']}');
    return Group.fromJson(response['response'][0]);
  }

/*
  POSTS
  getPosts
  getAllAccessiblePosts
  getPostById
  createPost
  editPost
  removePost
  */
  Future<List<Post>> getAllAccessiblePosts({int limit: 25}) async {
    logger.info(
        'Fetch all posts currently accessible by the user with a limit of $limit');
    Map<String, dynamic> response =
        await rawApiConnector.getAllAccessiblePosts(limit: limit);

    List<Post> postList = <Post>[];
    for (Map<String, dynamic> elem in response['response']) {
      postList.add(Post.fromJson(elem));
    }
    logger.info(
        'Fetched all posts currently accessible by the user with a limit of $limit. Count: ${postList.length} successfully');
    logger.fine('Posts: ${postList.map((p) => p.id.value).join(", ")}');
    logger.finer('Data: ${response['response']}');
    return postList;
  }

  Future<List<Post>> getAllAccessiblePostsBeforeCurrentOne(
      {required Id postId, int limit: 25}) async {
    logger.info(
        'Fetch all posts currently accessible by the user with a limit of $limit');
    Map<String, dynamic> response = await rawApiConnector
        .getAllAccessiblePostsBeforeId(postId.value, limit: limit);

    List<Post> postList = <Post>[];
    for (Map<String, dynamic> elem in response['response']) {
      postList.add(Post.fromJson(elem));
    }
    logger.info(
        'Fetched all posts before post ${postId.value} currently accessible by the user with a limit of $limit. Count: ${postList.length} successfully');
    logger.fine('Posts: ${postList.map((p) => p.id.value).join(", ")}');
    logger.finer('Data: ${response['response']}');
    return postList;
  }

  Future<List<Post>> getAllAccessiblePostsAroundCurrentOne(
      {required Id postId, int limit: 25}) async {
    logger.info(
        'Fetch all posts currently accessible by the user with a limit of $limit');
    Map<String, dynamic> response = await rawApiConnector
        .getAllAccessiblePostsAroundId(postId.value, limit: limit);

    List<Post> postList = <Post>[];
    for (Map<String, dynamic> elem in response['response']) {
      postList.add(Post.fromJson(elem));
    }
    logger.info(
        'Fetched all posts around post ${postId.value} currently accessible by the user with a limit of $limit. Count: ${postList.length} successfully');
    logger.fine('Posts: ${postList.map((p) => p.id.value).join(", ")}');
    logger.finer('Data: ${response['response']}');
    return postList;
  }

  Future<List<Post>> getAllAccessiblePostsAfterCurrentOne(
      {required Id postId, int limit: 25}) async {
    logger.info(
        'Fetch all posts currently accessible by the user with a limit of $limit');
    Map<String, dynamic> response = await rawApiConnector
        .getAllAccessiblePostsAfterId(postId.value, limit: limit);

    List<Post> postList = <Post>[];
    for (Map<String, dynamic> elem in response['response']) {
      postList.add(Post.fromJson(elem));
    }
    logger.info(
        'Fetched all posts after post ${postId.value} currently accessible by the user with a limit of $limit. Count: ${postList.length} successfully');
    logger.fine('Posts: ${postList.map((p) => p.id.value).join(", ")}');
    logger.finer('Data: ${response['response']}');
    return postList;
  }

  Future<List<Post>> getPostsFromGroupBeforeCurrentOne(
      {required Id groupId, required Id postId, int limit = 20}) async {
    logger.info(
        'Fetch all posts after post with post id of $postId and group id of $groupId and limit of $limit');
    Map<String, dynamic> response = await rawApiConnector
        .getPostsFromGroupBeforeId(groupId.value, postId.value, limit: limit);
    List<Post> postList = <Post>[];
    for (Map<String, dynamic> elem in response['response']) {
      postList.add(Post.fromJson(elem));
    }
    logger.info(
        'Fetched all posts from group ${groupId.value} before post ${postId.value} with a limit of $limit. Count: ${postList.length} successfully!');
    logger.fine('Posts: ${postList.map((p) => p.id.value).join(", ")}');
    logger.finer('Data: ${response['response']}');
    return postList;
  }

  Future<List<Post>> getPostsFromGroupAfterCurrentOne(
      {required Id groupId, required Id postId, int limit = 20}) async {
    logger.info(
        'Fetch all posts after post with post id of $postId and group id of $groupId and limit of $limit');
    Map<String, dynamic> response = await rawApiConnector
        .getPostsFromGroupAfterId(groupId.value, postId.value, limit: limit);
    List<Post> postList = <Post>[];
    for (Map<String, dynamic> elem in response['response']) {
      postList.add(Post.fromJson(elem));
    }
    logger.info(
        'Fetched all posts from group ${groupId.value} after post ${postId.value} with a limit of $limit. Count: ${postList.length} successfully!');
    logger.fine('Posts: ${postList.map((p) => p.id.value).join(", ")}');
    logger.finer('Data: ${response['response']}');
    return postList;
  }

  Future<List<Post>> getPostsFromGroupAroundCurrentOne(
      {required Id groupId, required Id postId, int limit = 20}) async {
    logger.info(
        'Fetch all posts around post with post id of $postId and group id of $groupId and limit of $limit');
    Map<String, dynamic> response = await rawApiConnector
        .getPostsFromGroupAroundId(groupId.value, postId.value, limit: limit);
    List<Post> postList = <Post>[];
    for (Map<String, dynamic> elem in response['response']) {
      postList.add(Post.fromJson(elem));
    }
    logger.info(
        'Fetched all posts from group ${groupId.value} around post ${postId.value} with a limit of $limit. Count: ${postList.length} successfully!');
    logger.fine('Posts: ${postList.map((p) => p.id.value).join(", ")}');
    logger.finer('Data: ${response['response']}');
    return postList;
  }

  Future<List<Post>> getPostsFromGroup(
      {required Id groupId, int limit: 25}) async {
    logger.info(
        'Fetch all posts from group ${groupId.value} accessible by the currently logged in user with a limit of $limit');
    Map<String, dynamic> response =
        await rawApiConnector.getPostsByGroup(groupId.value, limit: limit);

    List<Post> postList = <Post>[];
    for (Map<String, dynamic> elem in response['response']) {
      postList.add(Post.fromJson(elem));
    }
    logger.info(
        'Fetched all posts from group ${groupId.value} accessible by the currently logged in user with a limit of $limit. Count: ${postList.length} successfully!');
    logger.fine('Posts: ${postList.map((p) => p.id.value).join(", ")}');
    logger.finer('Data: ${response['response']}');
    return postList;
  }

  Future<Post> getPostById({required Id postId, required Id groupId}) async {
    logger.info('Fetch post ${postId.value}');
    Map<String, dynamic> response =
        await rawApiConnector.getPostById(postId.value, groupId.value);

    logger.info('Fetched post ${postId.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return Post.fromJson(response['response'][0]);
  }

  Future<Post> createPost(
      {required Id groupId,
      required String title,
      required List<Id> tags,
      required String details,
      required ApiColor color,
      required DateTime expirationTime}) async {
    logger.info('Create post');
    List<String?> tagIds = <String?>[];
    (await tags).forEach((t) {
      tagIds.add(t.value);
    });
    Map<String, dynamic> response = await rawApiConnector.postPost(
        groupId.value,
        title,
        tagIds,
        details,
        color.toObject(),
        expirationTime);

    logger.info('Created post successfully!');
    logger.fine(
        'Post: ${groupId.value}, title: ${title}, tags: [${tags.map((p) => p.value).join(", ")}], details: ${details}, color: ${color.toString()}, expirationTime: ${expirationTime.toString()}');
    logger.finer('Data: ${response['response']}');
    return Post.fromJson(response['response'][0]);
  }

  Future<Post> updatePost(
      {required Id postId,
      required Id groupId,
      String? title,
      List<Id>? tags,
      String? details,
      ApiColor? color,
      DateTime? expirationTime}) async {
    logger.info('Update post ${postId.value}');
    Map<String, dynamic> response = await rawApiConnector.patchPost(
        groupId.value,
        postId.value,
        title,
        tags?.map((e) => e.value).toList(),
        details,
        color?.toObject(),
        expirationTime);

    logger.info('Updated post ${postId.value} successfully!');
    logger.fine(
        'Post: ${groupId.value}, title: ${title}, tags: [${tags?.map((p) => p.value).join(", ")}], details: ${details}, color: ${color?.toString()}, expirationTime: ${expirationTime.toString()}');
    logger.finer('Data: ${response['response']}');
    return Post.fromJson(response['response'][0]);
  }

  Future<bool> completePost({required Id groupId, required Id postId}) async {
    logger.info('Complete post ${postId.value}');
    await rawApiConnector.completePost(
      groupId.value,
      postId.value,
    );

    logger.info('Completed post ${postId.value} successfully');
    return true;
  }

  Future<bool> uncompletePost({required Id groupId, required Id postId}) async {
    logger.info('Uncomplete post ${postId}');
    await rawApiConnector.uncompletePost(
      groupId.value,
      postId.value,
    );

    logger.info('Uncompleted post ${postId.value} successfully!');
    return true;
  }

  Future<bool> removePost({required Id postId, required Id groupId}) async {
    logger.info('Remove post ${postId.value} from group ${groupId.value}');
    Map<String, dynamic> response =
        await rawApiConnector.deletePost(groupId.value, postId.value);

    logger.info(
        'Removed post ${postId.value} from group ${groupId.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return true;
  }

/*
  TAGS
    getTagsByGroup
    getTagByGroupAndId
    createTag
    modifyTag
    deleteTag
   */
  Future<List<Tag>> getTags({required Id groupId,
      int limit: 25, int offset: 0}) async {
    logger.info(
        'Fetch all tags from group ${groupId.value} accessible by the currently logged in user with a limit of $limit and an offset of $offset');
    Map<String, dynamic> response = await rawApiConnector.getTags(groupId.value,
        limit: limit, offset: offset);

    List<Tag> tagList = <Tag>[];
    for (Map<String, dynamic> elem in response['response']) {
      tagList.add(Tag.fromJson(elem));
    }
    logger.info(
        'Fetched all tags from group ${groupId.value} accessible by the currently logged in user with a limit of $limit and an offset of $offset. Count: ${tagList.length} successfully!');
    logger.fine('Tags: ${tagList.map((t) => t.id.value).join(", ")}');
    logger.finer('Data: ${response['response']}');
    return tagList;
  }

  Future<Tag> getTag({required Id tagId, required Id groupId}) async {
    logger.info('Fetch tag ${tagId.value} from group ${groupId.value}');
    Map<String, dynamic> response =
        await rawApiConnector.getTag(groupId.value, tagId.value);

    logger.info(
        'Fetched tag ${tagId.value} from group ${groupId.value} successfully');
    logger.finer('Data: ${response['response']}');
    return Tag.fromJson(response['response'][0]);
  }

  Future<Tag> createTag({required Id groupId, required String name, required ApiColor color}) async {
    logger.info('Create tag');
    Map<String, dynamic> response = await rawApiConnector.postTag(
        await groupId.value, name, color.toObject());

    logger.info('Created tag successfully');
    logger.fine(
        'Group: ${groupId}, name: ${name}, color: ${color.toString()}');
    logger.finer('Data: ${response['response']}');
    return Tag.fromJson(response['response'][0]);
  }

  Future<Tag> updateTag({required Id tagId, required Id groupId, String? name, ApiColor? color}) async {
    logger.info('Update tag ${tagId.value}');
    Map<String, dynamic> response = await rawApiConnector.patchTag(
        groupId.value, tagId.value, name, color?.toObject());

    logger.info('Updated tag ${tagId.value} successfully!');
    logger.fine('Name: ${name}, color: ${color.toString()}');
    logger.finer('Data: ${response['response']}');
    return Tag.fromJson(response['response'][0]);
  }

  Future<bool> removeTag({required Id tagId, required Id groupId}) async {
    logger.info('Remove tag ${tagId.value} from group ${groupId.value}');
    Map<String, dynamic> response =
        await rawApiConnector.deleteTag(groupId.value, tagId.value);

    logger.info(
        'Removed tag ${tagId.value} from group ${groupId.value} successfully!');
    logger.finer('Data: ${response['response']}');
    return true;
  }

  /* MEDIA
    getUserAvatar
  */
  Future<String> getUserAvatarUrl({required Id userId}) async {
    return await this.getAPIUrl() + "/media/avatars/" + userId.value;
  }
}
