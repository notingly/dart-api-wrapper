part of notingly_api.exceptions;

class NotEnoughPermissionsException implements ApiException {
  final String cause;
  final int code;

  NotEnoughPermissionsException(
      {this.cause = "You have not enough permissions to perform this action",
      this.code = 403});
}
