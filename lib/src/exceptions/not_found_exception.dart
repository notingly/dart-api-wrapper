part of notingly_api.exceptions;

class NotFoundException implements ApiException {
  final String cause;
  final int code;

  NotFoundException({this.cause = "Requested resource not found!", this.code = 404});
}
