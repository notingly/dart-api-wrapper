library notingly_api.exceptions;

part 'authentication_failed_exception.dart';
part 'authentication_required_exception.dart';
part 'code_exception.dart';
part 'action_failed_exception.dart';
part 'decode_exception.dart';
part 'not_found_exception.dart';
part 'invalid_input_exception.dart';
part 'jwt_expired_exception.dart';
part 'maintenance_exception.dart';
part 'not_enough_permissions_exception.dart';
part 'server_error_exception.dart';
part 'not_all_information_provided_in_class_exception.dart';
part 'notingly_api_exception.dart';
part 'network_error_exception.dart';