part of notingly_api.exceptions;

class AuthenticationFailedException implements ApiException {
  final String cause;
  final int code;

  AuthenticationFailedException(
      {this.cause = "Authentication failed", this.code = 403});
}
