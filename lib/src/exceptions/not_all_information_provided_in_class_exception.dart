part of notingly_api.exceptions;

class NotAllInformationProvidedInClass implements ApiException {
  final String cause;
  final int code;

  NotAllInformationProvidedInClass(
      {this.cause = "Not all information provided in class",
      this.code = 10500});
}
