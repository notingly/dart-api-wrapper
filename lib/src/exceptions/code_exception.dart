part of notingly_api.exceptions;

class CodeException implements ApiException {
  final String cause;
  final int code;

  CodeException({this.cause = "", this.code = -100});
}
