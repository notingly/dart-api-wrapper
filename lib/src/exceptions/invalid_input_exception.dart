part of notingly_api.exceptions;

class InvalidInputException implements ApiException {
  final String cause;

  final int code;

  InvalidInputException(
      {this.cause = "The given information was invalid!", this.code = 400});
}
