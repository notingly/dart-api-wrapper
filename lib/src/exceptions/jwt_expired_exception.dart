part of notingly_api.exceptions;

class JWTExpired implements ApiException {
  final String cause;
  final int code;

  JWTExpired({this.cause = "Expired JWT Token", this.code = 401});
}
