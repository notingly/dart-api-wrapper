part of notingly_api.exceptions;

class DecodeException implements ApiException {
  final String cause;
  final int code;

  DecodeException({this.cause = "", this.code = -101});
}
