part of notingly_api.exceptions;

abstract class ApiException implements Exception {
  final String cause;
  final int code;

  ApiException({required this.cause, required this.code});
}
