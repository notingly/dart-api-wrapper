part of notingly_api.exceptions;

class NetworkErrorException implements ApiException {
  final String cause;
  final int code;

  NetworkErrorException(
      {this.cause = "Could not establish a network connection!", this.code = 408}); // not sure about the status code
}
