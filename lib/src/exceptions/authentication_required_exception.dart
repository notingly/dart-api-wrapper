part of notingly_api.exceptions;

class AuthenticationRequiredException implements ApiException {
  final String cause;
  final int code;

  AuthenticationRequiredException(
      {this.cause = "Authentication Required", this.code = 401});
}
