part of notingly_api.exceptions;

class ServerErrorException implements ApiException {
  final String cause;
  final int code;

  ServerErrorException(
      {this.cause = "Cannot perform action!", this.code = 412});
}
