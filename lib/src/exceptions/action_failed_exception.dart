part of notingly_api.exceptions;

class ActionFailedException implements ApiException {
  final String cause;
  final int code;

  ActionFailedException({this.cause = "Cannot create, modify or delete entity!", this.code = 500});
}
