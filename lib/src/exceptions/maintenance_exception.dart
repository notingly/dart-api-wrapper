part of notingly_api.exceptions;

class MaintenanceException implements ApiException {
  final String cause;
  final int code;

  MaintenanceException(
      {this.cause = "We're currently under maintenance!", this.code = 503});
}
