part of notingly_api;

class Api {
  final Future<String> Function() getJWT;
  final Future<String> Function() getAPIUrl;
  final Dio httpClient;

  Api(
      {required this.getJWT,
      required this.getAPIUrl,
      required this.httpClient});

  /*
  Authentication
   */
  Future<Map<String, dynamic>> authenticate(
      String username, String password) async {
    try {
      Response response = await httpClient.post(
          await this.getAPIUrl() + "/auth/login",
          data: {"username": username, "password": password});
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> refreshJWTToken(String refreshToken) async {
    try {
      Response response = await httpClient.post(
          await this.getAPIUrl() + "/auth/refresh",
          data: {"jwt_refresh_token": refreshToken});
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

/*
  Groups
    Groups - GET
    Groups - GET by id
    Groups - POST
    Groups - PATCH by id
    Groups - DELETE by id
      Groups - POST add member by id
      Groups - PATCH update member by id
      Groups - DELETE remove member by id
   */
  Future<Map<String, dynamic>> getGroups({int limit: 25, int offset: 0}) async {
    try {
      Response response = await httpClient.get(
          await this.getAPIUrl() + "/groups?limit=$limit&offset=$offset",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}));
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getGroup(String? id) async {
    try {
      Response response = await httpClient.get(
          await this.getAPIUrl() + "/groups/$id",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}));
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> postGroup(
      String name, String type, String data, Object color) async {
    try {
      Response response = await httpClient.post(
          await this.getAPIUrl() + "/groups",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}),
          data: {"name": name, "type": type, "data": data, "color": color});
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> patchGroup(
      String id, String? name, String? data, Object? color) async {
    try {
      final httpData = {};
      httpData.addAllIfTrue({"name": name}, () => name != null);
      httpData.addAllIfTrue({"data": data}, () => data != null);
      httpData.addAllIfTrue({"color": color}, () => color != null);

      Response response = await httpClient.patch(
        await this.getAPIUrl() + "/groups/$id",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
        data: httpData,
      );
      return await Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> deleteGroup(String id) async {
    try {
      Response response = await httpClient.delete(
          await this.getAPIUrl() + "/groups/$id",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}));
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getGroupMembers(String groupId) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/groups/" + groupId + "/members",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getGroupMember(
      String groupId, String userId) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/groups/" + groupId + "/members/" + userId,
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> addGroupMember(
      String groupId, String userId) async {
    try {
      Response response = await httpClient.post(
          await this.getAPIUrl() + "/groups/" + groupId + "/members",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}),
          data: {'user': userId});
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> updateGroupMember(
      String groupId, String userId, int permissions) async {
    try {
      Response response = await httpClient.patch(
          await this.getAPIUrl() + "/groups/" + groupId + "/members/" + userId,
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}),
          data: {'permissions': permissions});
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> removeGroupMember(
      String groupId, String userId) async {
    try {
      Response response = await httpClient.delete(
        await this.getAPIUrl() + "/groups/" + groupId + "/members/" + userId,
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

/*
  Invite
    Invite - GET by id
    Invite - POST
    Invite - POST join
    Invite - DELETE by id
   */
  Future<Map<String, dynamic>> getInvite(String id) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/invites/$id",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> postInvite(String groupId) async {
    try {
      Response response = await httpClient.post(
          await this.getAPIUrl() + "/invites",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}),
          data: {"group": groupId});
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> postInviteJoin(String link) async {
    try {
      Response response = await httpClient.post(
        await this.getAPIUrl() + "/invites/join/$link",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

/*
  Posts
    Posts - GET all posts by group id
    Posts - GET by group and post id
    Posts - POST by group id
    Posts - PATCH by post and group id
    Posts - DELETE by post and group id
   */

  Future<Map<String, dynamic>> getAllAccessiblePosts({int limit: 25}) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/posts?limit=$limit&offset=",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getAllAccessiblePostsBeforeId(String postId,
      {int limit: 25}) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/posts?limit=$limit&before=$postId",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getAllAccessiblePostsAfterId(String postId,
      {int limit: 25}) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/posts?limit=$limit&after=$postId",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getAllAccessiblePostsAroundId(String postId,
      {int limit: 25}) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/posts?limit=$limit&around=$postId",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getPostsByGroup(String groupId,
      {int limit: 25}) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/groups/$groupId/posts?limit=$limit",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getPostById(
      String postId, String groupId) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/groups/$groupId/posts/$postId",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getPostsFromGroupAfterId(
      String groupId, String postId,
      {int limit = 20}) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() +
            "/groups/$groupId/posts?after=$postId&limit=$limit",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getPostsFromGroupBeforeId(
      String groupId, String postId,
      {int limit = 20}) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() +
            "/groups/$groupId/posts?before=$postId&limit=$limit",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getPostsFromGroupAroundId(
      String groupId, String postId,
      {int limit = 20}) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() +
            "/groups/$groupId/posts?around=$postId&limit=$limit",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> postPost(String groupId, String title, List tags,
      String? content, Object color, DateTime expirationTime) async {
    try {
      Response response = await httpClient.post(
          await this.getAPIUrl() + "/groups/$groupId/posts",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}),
          data: {
            "supportedFeatures": ["postComplete"],
            "title": title,
            "color": color,
            "tags": tags,
            "content": content,
            "expirationTime":
                DateFormat('yyyy-MM-dd kk:mm:ss').format(expirationTime)
          });
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> patchPost(
      String groupId,
      String postId,
      String? title,
      List<String>? tags,
      String? content,
      Object? color,
      DateTime? expirationTime) async {
    try {
      final httpData = {};
      httpData.addAllIfTrue({"title": title}, () => title != null);
      httpData.addAllIfTrue({"color": color}, () => color != null);
      httpData.addAllIfTrue({"tags": tags}, () => tags != null);
      httpData.addAllIfTrue({"content": content}, () => content != null);
      httpData.addAllIfTrue({
        "expirationTime": DateFormat('yyyy-MM-dd kk:mm:ss')
            .format(expirationTime ?? new DateTime.now())
      }, () => expirationTime != null);
      Response response = await httpClient.patch(
          await this.getAPIUrl() + "/groups/$groupId/posts/$postId",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}),
          data: httpData);
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<void> completePost(String groupId, String postId) async {
    try {
      Response response = await httpClient.patch(
        await this.getAPIUrl() + "/groups/$groupId/posts/$postId/complete",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return;
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<void> uncompletePost(String groupId, String postId) async {
    try {
      Response response = await httpClient.delete(
        await this.getAPIUrl() + "/groups/$groupId/posts/$postId/complete",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return;
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> deletePost(String groupId, String postId) async {
    try {
      Response response = await httpClient.delete(
        await this.getAPIUrl() + "/groups/$groupId/posts/$postId",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

/*
  User
    User - GET
    USER - GET self
    User - GET by id
    User - POST
    User - PATCH by id
    User - DELETE by id
   */

  Future<Map<String, dynamic>> getSelfUser() async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/self",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getUser(String id) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/users/$id",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> createUser(String username, String password,
      {List<String>? roles, String? code}) async {
    try {
      final Map<String, dynamic> data = {};
      data["username"] = username;
      data["password"] = password;
      data["roles"] = roles ?? ["ROLE_USER"];
      if (code != null) {
        data["code"] = code;
      }

      Response response = await httpClient.post(
          await this.getAPIUrl() + "/users",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}),
          data: data);
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> deleteUser(String id) async {
    try {
      Response response = await httpClient.delete(
        await this.getAPIUrl() + "/users/$id",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> updateUser(String id, String? password,
      String? displayName, List<int>? avatar) async {
    try {
      final Map<String, dynamic> httpData = {};
      httpData.addAllIfTrue({"password": password}, () => password != null);
      httpData.addAllIfTrue(
          {"displayName": displayName}, () => displayName != null);
      httpData.addAllIfTrue(
          {"avatar": UriData.fromBytes(avatar ?? [], mimeType: 'image/jpg').toString()},
          () => avatar != null);
      Response response = await httpClient.patch(
          await this.getAPIUrl() + "/users/$id",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}),
          data: httpData);
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  /*
  Tag
    Tag - GET
    Tag - GET by id
    Tag - POST
    Tag - PATCH by id
    Tag - DELETE by id
   */
  Future<Map<String, dynamic>> getTags(String groupId,
      {limit: 25, offset: 0}) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() +
            "/groups/$groupId/tags?limit=$limit&offset=$offset",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> getTag(String groupId, String tagId) async {
    try {
      Response response = await httpClient.get(
        await this.getAPIUrl() + "/groups/$groupId/tags/$tagId",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> postTag(
      String groupId, String name, Object color) async {
    try {
      Response response = await httpClient.post(
          await this.getAPIUrl() + "/groups/$groupId/tags",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}),
          data: {"name": name, "color": color});
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> patchTag(
      String groupId, String tagId, String? name, Object? color) async {
    try {
      final httpData = {};
      httpData.addAllIfTrue({"name": name}, () => name != null);
      httpData.addAllIfTrue({"color": color}, () => color != null);
      Response response = await httpClient.patch(
          await this.getAPIUrl() + "/groups/$groupId/tags/$tagId",
          options: Options(
              headers: {"Authorization": "Bearer " + await this.getJWT()}),
          data: httpData);
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }

  Future<Map<String, dynamic>> deleteTag(String groupId, String tagId) async {
    try {
      Response response = await httpClient.delete(
        await this.getAPIUrl() + "/groups/$groupId/tags/$tagId",
        options: Options(
            headers: {"Authorization": "Bearer " + await this.getJWT()}),
      );
      response = requestInterceptor(response);
      return Map<String, dynamic>.from(response.data);
    } catch (e) {
      exceptionsHandler(e);
    }
    throw new Exception("Unreachable code");
  }
}
