part of notingly_api;

final logger = Logger('NotinglyApi');

bool checkResponseStructure(content) {
  try {
    if (content.containsKey('errorDetail') &&
        content.containsKey('message') &&
        content.containsKey('response')) return true;
    throw new DecodeException();
  } catch (e) {
    logger.info("The response structure doesn't fulfill the requirements!");
    logger.finer("The following response was returned: " + content);
    throw new DecodeException();
  }
}

Response requestInterceptor(Response response) {
  if (response.requestOptions.responseType == ResponseType.json && response.statusCode != 204) {
    if (!response.data.containsKey("token") &&
        !response.data.containsKey("jwt_refresh_token")) {
      checkResponseStructure(response.data);
      response.data = Map<String, dynamic>.from(response.data);
      return response;
    }
  }
  return response;
}

void exceptionsHandler(Object exception) {
  logger.info("The request failed!");
  try {
    if (exception is DioError) {
      logger.finer("The request failed with the returned error message " + exception.error.toString());
      if(exception.error is SocketException || exception.response == null){
        throw new NetworkErrorException();
      };
      switch (exception.response?.data['errorDetail']) {
        case 'expiredToken':
          throw new JWTExpired();
        case 'authenticationFailed':
          throw new AuthenticationFailedException();
        case 'authenticationRequired':
          throw new AuthenticationRequiredException();
        case 'createEntityError':
          throw new ActionFailedException();
        case 'actionFailed':
          throw new ActionFailedException();
        case 'entityNotFound':
          throw new NotFoundException();
        case 'invalidInput':
          throw new InvalidInputException();
        case 'maintenance':
          throw new MaintenanceException();
        case 'notEnoughPermissions':
          throw new NotEnoughPermissionsException();
        case 'serverError':
          throw new ServerErrorException();
        default:
          throw CodeException(
              cause: exception.response?.data['errorDetail'],
              code: exception.response?.data['code']);
      }
    }
  } catch (e) {
    if (e is ApiException) {
      logger.fine("The thrown exception was " + e.toString());
      rethrow;
    }
    logger.fine("The thrown exception was " + e.toString() + " which was converted to a DecodeException!");
  }
  throw new DecodeException();
}
