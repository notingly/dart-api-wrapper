part of notingly_api;

extension MapExtension on Map {
  void addAllIfTrue<K, V>(Map<K, V> other, bool Function() validator) {
    if (validator()) {
      this.addAll(other);
    }
  }
}
