part of notingly_api.models;

class Tag {
  Id id;
  String name;
  Id group;
  ApiColor color;
  List<Id> posts;

  bool _empty;

  set empty(bool value) {
    _empty = value;
  }

  Tag(
      {required this.id,
      required this.name,
      required this.group,
      required this.color,
      required this.posts})
      : _empty = false;

  Tag.fromJson(Map json)
      : this.id = json.containsKey('id')
            ? Id(json['id'])
            : throw new DecodeException(),
        this.name = json.containsKey('name')
            ? json['name']
            : throw new DecodeException(),
        this.group = json.containsKey('group')
            ? Id(json['group'])
            : throw new DecodeException(),
        this.color = json.containsKey('color')
            ? ApiColor.fromJson(json['color'])
            : throw new DecodeException(),
        this.posts = json.containsKey('posts')
            ?  List.generate(json['posts'].length, (index) => Id(json['posts'][index]))
            : throw new DecodeException(),
        this._empty = false;

  bool createValidate() => name != "" && !group.empty;

  bool updateValidate() => !id.empty && name != "";
}
