part of notingly_api.models;

class ApiColor {
  final int r;
  final int g;
  final int b;

  ApiColor(int this.r, int this.g, int this.b);

  factory ApiColor.fromJson(Map json) => ApiColor(
      json.containsKey('r') ? json['r'] : throw new DecodeException(),
      json.containsKey('g') ? json['g'] : throw new DecodeException(),
      json.containsKey('b') ? json['b'] : throw new DecodeException());

  Object toObject() {
    return {'r': r, 'g': g, 'b': b};
  }

  @override
  String toString() => 'Color($r, $g, $b)';
}
