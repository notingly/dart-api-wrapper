part of notingly_api.models;

enum GroupType { SCHOOL_CLASS }

extension GroupTypeHelperFunctions on GroupType? {
  get string {
    switch (this) {
      case GroupType.SCHOOL_CLASS:
      default:
        return 'school_class';
    }
  }

  GroupType? fromString(String groupType) {
    switch (groupType) {
      case 'school_class':
        return GroupType.SCHOOL_CLASS;
      default:
        return this;
    }
  }
}
