part of notingly_api.models;

class Invite {
  Id id;
  Id group;
  String link;

  bool _empty;

  bool get empty => _empty;

  Invite.empty()
      : id = Id.empty(),
        group = Id.empty(),
        link = "",
        _empty = true;

  Invite({required this.id, required this.group, required this.link})
      : _empty = false;

  Invite.fromJson(Map json)
      : this.id = json.containsKey('id')
            ? Id(json['id'])
            : throw new DecodeException(),
        this.group = json.containsKey('group')
            ? Id(json['group'])
            : throw new DecodeException(),
        this.link = json.containsKey('link')
            ? json['link']
            : throw new DecodeException(),
        this._empty = false;

  bool createValidate() => !this.group.empty;
}
