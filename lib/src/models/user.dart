part of notingly_api.models;

class User {
  Id id;
  String username;
  String displayName;
  List<String> roles;
  List<Id> groups;

  bool _empty;

  User(
      {required this.id,
      required this.username,
      required this.displayName,
      required this.roles,
      required this.groups})
      : this._empty = false;

  bool get empty => _empty;

  User.fromJson(Map json)
      : this.id = json.containsKey('id')
            ? Id(json['id'])
            : throw new DecodeException(),
        this.username = json.containsKey('username')
            ? json['username']
            : throw new DecodeException(),
        this.displayName = json.containsKey('displayName')
            ? json['displayName']
            : throw new DecodeException(),
        this.roles =
            json.containsKey('roles') ? List<String>.from(json['roles']) : [],
        this.groups = json.containsKey('groups')
            ? List.generate(
                json['groups'].length, (index) => Id(json['groups'][index]))
            : [],
        this._empty = false;

  bool essentialValuesValidate() => !id.empty;
}
