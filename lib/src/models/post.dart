part of notingly_api.models;

class Post {
  Id id;
  String title;
  Id group;
  List<Id> tags;
  String details;
  ApiColor color;
  Id user;
  bool completed;
  DateTime creationTime;
  DateTime expirationTime;

  bool _empty;

  Post(
      {required this.id,
      required this.title,
      required this.group,
      required this.tags,
      required this.details,
      required this.color,
      required this.user,
      required this.creationTime,
      required this.completed,
      required this.expirationTime})
      : _empty = false;

  bool get empty => _empty;

  Post.fromJson(Map json)
      : id = json.containsKey('id')
            ? Id(json['id'])
            : throw new DecodeException(),
        title = json.containsKey('title')
            ? json['title']
            : throw new DecodeException(),
        group = json.containsKey('group')
            ? Id(json['group'])
            : throw new DecodeException(),
        color = json.containsKey('color')
            ? new ApiColor.fromJson(json['color'])
            : throw new DecodeException(),
        tags = json.containsKey('tags')
            ? List.generate(json['tags'].length, (index) => Id(json['tags'][index]))
            : throw new DecodeException(),
        details = json.containsKey('content')
            ? json['content']
            : throw new DecodeException(),
        completed = json.containsKey('completed')
            ? json['completed']
            : throw new DecodeException(),
        user = json.containsKey('user')
            ? Id(json['user'])
            : throw new DecodeException(),
        creationTime = json.containsKey('creationTime')
            ? DateTime.parse(json['creationTime']['date'])
            : throw new DecodeException(),
        expirationTime = json.containsKey('expirationTime')
            ? DateTime.parse(json['expirationTime']['date'])
            : throw new DecodeException(),
        _empty = false;

  bool createValidate() =>
      title != "" &&
      !group.empty;

  bool updateValidate() =>
      !id.empty &&
      title != "" &&
      !group.empty;
}
