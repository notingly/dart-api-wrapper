part of notingly_api.models;

class Group {
  Id id;
  String name;
  GroupType type;
  List<Id> tags;
  Id invite;
  String data;
  ApiColor color;
  Id owner;

  bool _empty;

  bool get empty => _empty;

  Group(
      {required this.id,
      required this.name,
      required this.type,
      required this.tags,
      required this.invite,
      required this.data,
      required this.color,
      required this.owner})
      : _empty = false;

  Group.empty()
      : this.id = Id.empty(),
        this.name = "",
        this.data = "",
        this.type = GroupType.SCHOOL_CLASS,
        this.tags = [],
        this.invite = Id.empty(),
        this.owner = Id.empty(),
        this.color = ApiColor(0, 0, 0),
        this._empty = true;

  Group.fromJson(Map json)
      : this.id = json.containsKey('id')
            ? Id(json['id'])
            : throw new DecodeException(),
        this.name = json.containsKey('name')
            ? json['name']
            : throw new DecodeException(),
        this.type = json.containsKey('type')
            ? json['type'] == "school_class"
                ? GroupType.SCHOOL_CLASS
                : throw new DecodeException()
            : throw new DecodeException(),
        this.tags = json.containsKey('tags')
            ? List.generate(json['tags'].length, (index) => Id(json['tags'][index]))
            : throw new DecodeException(),
        this.invite = json.containsKey('invite')
            ? Id(json['invite'])
            : throw new DecodeException(),
        this.data = json.containsKey('data')
            ? json['data']
            : throw new DecodeException(),
        this.color = json.containsKey('color')
            ? ApiColor.fromJson(json['color'])
            : throw new DecodeException(),
        this.owner = json.containsKey('owner')
            ? Id(json['owner'])
            : throw new DecodeException(),
        this._empty = false;

  bool createValidate() => name != "";

  bool updateValidate() => !id.empty && name != "";

  bool essentialValuesValidate() => !id.empty;
}
