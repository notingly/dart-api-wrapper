part of notingly_api.models;

class GroupMember {
  User user;
  GroupPermissions permissions;

  bool _empty;

  bool get empty => _empty;

  GroupMember({required this.user, required this.permissions}) : _empty = false;

  GroupMember.empty()
      : this._empty = true,
        this.user = User(id: Id.empty(), username: "", displayName: "", roles: [], groups: []),
        this.permissions = GroupPermissions.empty();

  GroupMember.fromJson(Map json)
      : this.user = json.containsKey('user')
            ? User.fromJson(json['user'])
            : throw new DecodeException(),
        this.permissions = json.containsKey('permissions')
            ? GroupPermissions.fromInt(json['permissions'])
            : throw new DecodeException(),
        this._empty = false;

}
