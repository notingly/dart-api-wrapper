part of notingly_api.models;

class Id {
  final String value;
  bool _empty;

  bool get empty => _empty;

  Id(this.value) : this._empty = false;

  Id.empty()
      : this.value = "",
        this._empty = true;
}
