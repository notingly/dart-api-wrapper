part of notingly_api.models;

class GroupPermissions {
  static const CREATE_POSTS = 0x0000000001;
  static const MANAGE_POSTS = 0x0000000002;
  static const MANAGE_GROUP = 0x0000000004;
  static const ADMINISTRATE_GROUP = 0x0000000008;

  bool administrateGroup;
  bool manageGroup;
  bool createPosts;
  bool managePosts;

  GroupPermissions(this.administrateGroup, this.manageGroup, this.createPosts,
      this.managePosts);

  GroupPermissions.empty()
      : this.administrateGroup = false,
        this.manageGroup = false,
        this.createPosts = false,
        this.managePosts = false;

  GroupPermissions.fromInt(int value)
      : administrateGroup = (value & ADMINISTRATE_GROUP) == ADMINISTRATE_GROUP,
        manageGroup = (value & ADMINISTRATE_GROUP) == ADMINISTRATE_GROUP,
        createPosts = (value & ADMINISTRATE_GROUP) == ADMINISTRATE_GROUP,
        managePosts = (value & ADMINISTRATE_GROUP) == ADMINISTRATE_GROUP;

  int toInt() {
    int value = 0x0;
    if (this.createPosts) {
      value |= CREATE_POSTS;
    }
    if (this.managePosts) {
      value |= MANAGE_POSTS;
    }
    if (this.manageGroup) {
      value |= MANAGE_GROUP;
    }
    if (this.administrateGroup) {
      value |= ADMINISTRATE_GROUP;
    }
    return value;
  }
}
