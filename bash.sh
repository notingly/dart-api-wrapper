#!/usr/bin/env sh
flutter test --coverage
genhtml coverage/lcov.info -o coverage
