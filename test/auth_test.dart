import 'package:notingly_api/notingly_api.dart';
import 'package:test/test.dart';

import 'dio_interceptor.dart';

void main() async {
  bool expired = false;
  bool invalidLogin = false;

  final api = new NotinglyApi(
    getJWT: () {
      if (expired) {
        return Future.value('JWT_EXPIRED');
      }
      if (invalidLogin) {
        return Future.value('JWT_INVALID_CREDENTIALS');
      }
      return Future.value("jwt");
    },
    getJWTRefresh: () {
      expired = false;
      return Future.value('jwtRefresh');
    },
    getAPIUrl: () => Future.value('https://api.test'),
    setJWT: (_) => Future.value(),
    setJWTRefresh: (_) => Future.value(),
    enableLoggingInConsole: false,
  );
  api.httpClient.interceptors.add(createDioInterceptor());

  group('Authentication', () {
    test('login with valid credentials', () async {
      User user = await api.login(username: 'username', password: 'password');
      expect(user is User, isTrue);
    });
    test('login with invalid credentials', () async {
      invalidLogin = true;
      expect(
          () async =>
              await api.login(username: 'no username', password: 'password'),
          throwsA(isA<AuthenticationFailedException>()));
    });
    test('authenticate with jwt successfully', () async {
      invalidLogin = false;
      expired = false;
      User user = await api.authenticateWithJWT();
      expect(user is User, isTrue);
    });
    test('authenticate with expired jwt', () async {
      invalidLogin = false;
      expired = true;
      User user = await api.authenticateWithJWT();
      expect(user is User, isTrue);
    });
  });
}
