import 'package:notingly_api/notingly_api.dart';
import 'package:test/test.dart';

import 'dio_interceptor.dart';

void main() async {
  final api = new NotinglyApi(
    getJWT: () => Future.value("jwt"),
    getJWTRefresh: () => Future.value('jwtRefresh'),
    getAPIUrl: () => Future.value('https://api.test'),
    setJWT: (_) => Future.value(),
    setJWTRefresh: (_) => Future.value(),
    enableLoggingInConsole: false,
  );
  api.httpClient.interceptors.add(createDioInterceptor());

  group('User', () {
    group('get user by id', () {
      test('getting an user successfully', () async {
        User user = await api.getUser(id: Id('userId'));
        expect(user.id.value, equals('vzD11kJEp6T0gyB4UoKgn'));
        expect(user.username, equals('TestUser1'));
      });
      test('getting an user that does not exist', () async {
        expect(() async => await api.getUser(id: Id('notFound')),
            throwsA(isA<NotFoundException>()));
      });
    });
    group('get logged in user', () {
      test('getting logged in user successfully', () async {
        User user = await api.getSelfUser();
        expect(user.id.value, equals("vzD11kJEp6T0gyB4UoKgn"));
        expect(user.username, equals("TestUser1"));
      });
    });
    group('remove user', () {
      test('removing user successfully', () async {
        expect(await api.removeUser(id: Id('userId')), isTrue);
      });
      test('remove user that does not exist', () async {
        expect(() async => await api.removeUser(id: Id('notFound')),
            throwsA(isA<NotFoundException>()));
      });
    });
  });
}
