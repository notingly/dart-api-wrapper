import 'package:notingly_api/notingly_api.dart';
import 'package:test/test.dart';

import 'dio_interceptor.dart';

void main() async {
  final api = new NotinglyApi(
    getJWT: () => Future.value("jwt"),
    getJWTRefresh: () => Future.value('jwtRefresh'),
    getAPIUrl: () => Future.value('https://api.test'),
    setJWT: (_) => Future.value(),
    setJWTRefresh: (_) => Future.value(),
    enableLoggingInConsole: false,
  );
  api.httpClient.interceptors.add(createDioInterceptor());

  group('Post', () {
    group('get all accessible posts', () {
      group('get all accessible posts around post', () {
        test('getting all accessible posts around post successfully', () async {
          List<Post> posts = await api.getAllAccessiblePostsAroundCurrentOne(
              postId: Id("postId"));
          expect(posts[0].id.value, equals('vzUvMBNGhCTl4jUyGXfk0'));
          expect(posts[0].user.value, equals('vzUvMBljOJDY2M1wENoLw'));
          expect(posts[0].group.value, equals('vzUvMBGRaqoxY87XSTTou'));
          expect(posts[0].title, equals('23.10.19 Mathe'));
          expect(posts[0].color.r, equals(125));
          expect(posts[0].color.g, equals(255));
          expect(posts[0].color.b, equals(12));
          expect(posts[0].details, equals('Mathetest mit dem Thema rechnen'));
        });
        test('getting all accessible posts around post without valid postId',
            () async {
          expect(
              () async => await api.getAllAccessiblePostsAroundCurrentOne(
                  postId: Id("notFound")),
              throwsA(
                isA<NotFoundException>(),
              ));
        });
      });
      group('get all accessible posts before post', () {
        test('getting all accessible posts before post successfully', () async {
          List<Post> posts = await api.getAllAccessiblePostsBeforeCurrentOne(
              postId: Id("postId"));
          expect(posts[0].id.value, equals('vzUvMBNGhCTl4jUyGXfk0'));
          expect(posts[0].user.value, equals('vzUvMBljOJDY2M1wENoLw'));
          expect(posts[0].group.value, equals('vzUvMBGRaqoxY87XSTTou'));
          expect(posts[0].title, equals('23.10.19 Mathe'));
          expect(posts[0].color.r, equals(125));
          expect(posts[0].color.g, equals(255));
          expect(posts[0].color.b, equals(12));
          expect(posts[0].details, equals('Mathetest mit dem Thema rechnen'));
        });
        test('getting all accessible posts before post without valid postId',
            () async {
          expect(
              () async => await api.getAllAccessiblePostsBeforeCurrentOne(
                  postId: Id("notFound")),
              throwsA(
                isA<NotFoundException>(),
              ));
        });
      });
      group('get all accessible posts after post', () {
        test('getting all accessible posts after post successfully', () async {
          List<Post> posts = await api.getAllAccessiblePostsAfterCurrentOne(
              postId: Id("postId"));
          expect(posts[0].id.value, equals('vzUvMBNGhCTl4jUyGXfk0'));
          expect(posts[0].user.value, equals('vzUvMBljOJDY2M1wENoLw'));
          expect(posts[0].group.value, equals('vzUvMBGRaqoxY87XSTTou'));
          expect(posts[0].title, equals('23.10.19 Mathe'));
          expect(posts[0].color.r, equals(125));
          expect(posts[0].color.g, equals(255));
          expect(posts[0].color.b, equals(12));
          expect(posts[0].details, equals('Mathetest mit dem Thema rechnen'));
        });
        test('getting all accessible posts after post without valid postId',
            () async {
          expect(
              () async => await api.getAllAccessiblePostsAfterCurrentOne(
                  postId: Id("notFound")),
              throwsA(
                isA<NotFoundException>(),
              ));
        });
      });
    });
    group('get all accessible posts from group', () {
      group('get all accessible posts from group around post', () {
        test('getting all accessible posts from group around post successfully',
            () async {
          List<Post> posts = await api.getPostsFromGroupAroundCurrentOne(
              groupId: Id("groupId"), postId: Id("postId"));
          expect(posts[0].id.value, equals('vzUvMBNGhCTl4jUyGXfk0'));
          expect(posts[0].user.value, equals('vzUvMBljOJDY2M1wENoLw'));
          expect(posts[0].group.value, equals('vzUvMBGRaqoxY87XSTTou'));
          expect(posts[0].title, equals('23.10.19 Mathe'));
          expect(posts[0].color.r, equals(125));
          expect(posts[0].color.g, equals(255));
          expect(posts[0].color.b, equals(12));
          expect(posts[0].details, equals('Mathetest mit dem Thema rechnen'));
        });
        test(
            'getting all accessible posts from group around post without valid postId',
            () async {
          expect(
              () async => await api.getPostsFromGroupAroundCurrentOne(
                  groupId: Id("groupId"), postId: Id("notFound")),
              throwsA(
                isA<NotFoundException>(),
              ));
        });
      });
      group('get all accessible posts from group before post', () {
        test('getting all accessible posts from group before post successfully',
            () async {
          List<Post> posts = await api.getPostsFromGroupBeforeCurrentOne(
              groupId: Id("groupId"), postId: Id("postId"));
          expect(posts[0].id.value, equals('vzUvMBNGhCTl4jUyGXfk0'));
          expect(posts[0].user.value, equals('vzUvMBljOJDY2M1wENoLw'));
          expect(posts[0].group.value, equals('vzUvMBGRaqoxY87XSTTou'));
          expect(posts[0].title, equals('23.10.19 Mathe'));
          expect(posts[0].color.r, equals(125));
          expect(posts[0].color.g, equals(255));
          expect(posts[0].color.b, equals(12));
          expect(posts[0].details, equals('Mathetest mit dem Thema rechnen'));
        });
        test(
            'getting all accessible posts from group before post without valid postId',
            () async {
          expect(
              () async => await api.getPostsFromGroupBeforeCurrentOne(
                  groupId: Id("groupId"), postId: Id("notFound")),
              throwsA(
                isA<NotFoundException>(),
              ));
        });
      });
      group('get all accessible posts from group after post', () {
        test('getting all accessible posts from group after post successfully',
            () async {
          List<Post> posts = await api.getPostsFromGroupAfterCurrentOne(
              groupId: Id("groupId"), postId: Id("postId"));
          expect(posts[0].id.value, equals('vzUvMBNGhCTl4jUyGXfk0'));
          expect(posts[0].user.value, equals('vzUvMBljOJDY2M1wENoLw'));
          expect(posts[0].group.value, equals('vzUvMBGRaqoxY87XSTTou'));
          expect(posts[0].title, equals('23.10.19 Mathe'));
          expect(posts[0].color.r, equals(125));
          expect(posts[0].color.g, equals(255));
          expect(posts[0].color.b, equals(12));
          expect(posts[0].details, equals('Mathetest mit dem Thema rechnen'));
        });
        test(
            'getting all accessible posts from group after post without valid postId',
            () async {
          expect(
              () async => await api.getPostsFromGroupAfterCurrentOne(
                  groupId: Id("groupId"), postId: Id("notFound")),
              throwsA(
                isA<NotFoundException>(),
              ));
        });
      });
    });
    group('get post by id from group', () {
      test('get post by id from group successfully', () async {
        Post posts =
            await api.getPostById(postId: Id("postId"), groupId: Id("groupId"));
        expect(posts.id.value, equals('vzUvMBNGhCTl4jUyGXfk0'));
        expect(posts.user.value, equals('vzUvMBljOJDY2M1wENoLw'));
        expect(posts.group.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(posts.title, equals('23.10.19 Mathe'));
        expect(posts.color.r, equals(125));
        expect(posts.color.g, equals(255));
        expect(posts.color.b, equals(12));
        expect(posts.details, equals('Mathetest mit dem Thema rechnen'));
      });
      test('get not existing post by id from group', () async {
        expect(
            () async => await api.getPostById(
                postId: Id("postId"), groupId: Id("notFound")),
            throwsA(isA<NotFoundException>()));
      });
    });
    group('create post', () {
      test('create post successfully', () async {
        Post post = await api.createPost(
          title: "Created title",
          groupId: Id("groupId"),
          tags: [Id("tagId")],
          details: "Details",
          color: ApiColor(10, 255, 123),
          expirationTime: new DateTime.now(),
        );
        expect(post.id.value, equals('vzUvMBNGhCTl4jUyGXfk0'));
        expect(post.user.value, equals('vzUvMBljOJDY2M1wENoLw'));
        expect(post.group.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(post.title, equals('23.10.19 Mathe'));
        expect(post.color.r, equals(125));
        expect(post.color.g, equals(255));
        expect(post.color.b, equals(12));
        expect(post.details, equals('Mathetest mit dem Thema rechnen'));
      });
      test('update post for not existing group', () async {
        expect(
          () async => await api.updatePost(
            postId: Id("postId"),
            title: "Created title",
            groupId: Id("notFound"),
            tags: [Id("tagId")],
            details: "Details",
            color: ApiColor(10, 255, 123),
            expirationTime: new DateTime.now(),
          ),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('update post', () {
      test('update post successfully', () async {
        Post post = await api.updatePost(
          postId: Id("postId"),
          title: "Created title",
          groupId: Id("groupId"),
          tags: [Id("tagId")],
          details: "Details",
          color: ApiColor(10, 255, 123),
          expirationTime: new DateTime.now(),
        );
        expect(post.id.value, equals('vzUvMBNGhCTl4jUyGXfk0'));
        expect(post.user.value, equals('vzUvMBljOJDY2M1wENoLw'));
        expect(post.group.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(post.title, equals('23.10.19 Mathe'));
        expect(post.color.r, equals(125));
        expect(post.color.g, equals(255));
        expect(post.color.b, equals(12));
        expect(post.details, equals('Mathetest mit dem Thema rechnen'));
      });
      test('update post for not existing group', () async {
        expect(
          () async => await api.updatePost(
            postId: Id("postId"),
            title: "Created title",
            groupId: Id("notFound"),
            tags: [Id("tagId")],
            details: "Details",
            color: ApiColor(10, 255, 123),
            expirationTime: new DateTime.now(),
          ),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('complete post', () {
      test('complete post successfully', () async {
        bool post = await api.completePost(
          postId: Id("postId"),
          groupId: Id("groupId"),
        );
        expect(post, isTrue);
      });
      test('complete not existing post', () async {
        expect(
          () async => await api.completePost(
            postId: Id("notFound"),
            groupId: Id("groupId"),
          ),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('uncomplete post', () {
      test('uncomplete post successfully', () async {
        bool post = await api.uncompletePost(
          postId: Id("postId"),
          groupId: Id("groupId"),
        );
        expect(post, isTrue);
      });
      test('uncomplete not existing post', () async {
        expect(
          () async => await api.uncompletePost(
            postId: Id("notFound"),
            groupId: Id("groupId"),
          ),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('delete post', () {
      test('delete post successfully', () async {
        expect(
            await api.removePost(postId: Id("postId"), groupId: Id("groupId")),
            isTrue);
      });
      test('delete not existing post', () async {
        expect(
          () async => await api.removePost(
              postId: Id("notFound"), groupId: Id("groupId")),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
  });
}
