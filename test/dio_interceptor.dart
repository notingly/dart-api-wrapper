import 'package:dio/dio.dart';

InterceptorsWrapper createDioInterceptor() {
  return InterceptorsWrapper(
    onRequest: (RequestOptions options, RequestInterceptorHandler handler) =>
        _requestInterceptor(options, handler),
  );
}

void _requestInterceptor(
  RequestOptions options,
  RequestInterceptorHandler handler,
) {
  const authenticationFailed = {
    "errorDetail": "authenticationFailed",
    "message": "",
    "response": []
  };

  const expiredToken = {
    "errorDetail": "expiredToken",
    "message": "",
    "response": []
  };

  const notFound = {
    "errorDetail": "entityNotFound",
    "message": "Requested entity not found!",
    "response": []
  };

  if (options.headers['Authorization'] != null &&
      options.headers['Authorization'] != 'Bearer jwt') {
    var authHeader = options.headers['Authorization'].replaceAll('Bearer ', '');
    var data = {};
    switch (authHeader) {
      case 'JWT_EXPIRED':
        data = expiredToken;
        break;
      case 'JWT_INVALID_CREDENTIALS':
        data = authenticationFailed;
        break;
    }
    return handler.reject(DioError(requestOptions: options, response: Response(requestOptions: options,data: data)), true);
  }

  switch (options.uri.path) {
    case '/invites/join/notFound':
    case '/invites/notFound':
    case '/users/notFound':
    case '/groups/notFound':
    case '/groups/notFound/tags':
    case '/groups/notFound/tags/notFound':
    case '/groups/groupId/tags/notFound':
    case '/groups/notFound/posts':
    case '/groups/notFound/posts/postId':
    case '/groups/groupId/posts/notFound':
    case '/groups/groupId/members/notFound':
    case '/groups/notFound/members/userId':
    case '/groups/notFound/members':
    case '/groups/notFound/members/self':
    case '/groups/groupId/members/notFound':
    case '/groups/notFound/members/userId':
    case '/groups/notFound/members/notFound':
    case '/groups/notFound/posts/postId/complete':
    case '/groups/groupId/posts/notFound/complete':
    case '/groups/notFound/posts/postId/uncomplete':
    case '/groups/groupId/posts/notFound/uncomplete':
      return handler.reject(
          DioError(requestOptions: options, response: Response(requestOptions: options,data: notFound)), true);
  }

  if ((options.uri.queryParameters.containsKey("before") &&
          options.uri.queryParameters["before"] == "notFound") ||
      (options.uri.queryParameters.containsKey("around") &&
          options.uri.queryParameters["around"] == "notFound") ||
      (options.uri.queryParameters.containsKey("after") &&
          options.uri.queryParameters["after"] == "notFound")) {
    return handler.reject(
        DioError(requestOptions: options, response: Response(requestOptions: options,data: notFound)), true);
  }

  if (options.method == "DELETE" ||
      (options.method == "POST" &&
          options.uri.path == '/groups/groupId/members') ||
      options.uri.path == '/groups/groupId/posts/postId/complete') {
    return handler.resolve(
        Response(
            requestOptions: options,
            data: {"errorDetail": "", "message": "", "response": []}),
        true);
  }

  var data = {};
  switch (options.uri.path) {
    case '/auth/login':
    case '/auth/refresh':
      data = {'token': 'JWT', 'jwt_refresh_token': 'REFRESH_TOKEN'};
      break;
    case '/self':
      data = {
        "errorDetail": "",
        "message": "",
        "response": [
          {
            "id": "vzD11kJEp6T0gyB4UoKgn",
            "username": "TestUser1",
            "displayName": "Test User 1",
            "roles": ["ROLE_USER"],
            "posts": ["vzD11k7FGLiREixiMuqVB", "vzD11k7FQryld3YCsSl25"],
            "groups": ["vzD11k79DGieFX4q4M3VV"]
          }
        ]
      };
      break;
    case '/users/userId':
      data = {
        "errorDetail": "",
        "message": "",
        "response": [
          {"id": "vzD11kJEp6T0gyB4UoKgn", "username": "TestUser1", "displayName": "Test User 1"}
        ]
      };
      break;
    case '/groups/groupId/tags':
    case '/groups/groupId/tags/tagId':
      data = {
        "errorDetail": "",
        "message": "",
        "response": [
          {
            "id": "vzUvMBNAYkJE2KlgJ5N5c",
            "name": "Important",
            "group": "vzUvMBGRaqoxY87XSTTou",
            "color": {"r": 125, "g": 255, "b": 12},
            "posts": [
              "vzUvMBNGZQNIzKZtbYH7g",
              "vzUvMBNGdMu8H0wryAvCa",
              "vzUvMBNGhCTl4jUyGXfk0",
              "vzUvMBNMQKB6PqNwoKUBU",
              "vzUvMBNMU9glLGT9sRpz6"
            ]
          },
        ]
      };
      break;
    case '/invites':
    case '/invites/inviteId':
      if (options.method == "POST" &&
          options.data.containsKey("group") &&
          options.data["group"] == "notFound") {
        handler.reject(
            DioError(requestOptions: options, response: Response(requestOptions: options,data: notFound)), true);
        return;
      }
      data = {
        "errorDetail": "",
        "message": "",
        "response": [
          {
            "id": "vzD11k7FDURPr7ug7mosn",
            "group": "vzD11k79DGieFX4q4M3VV",
            "link": "A8j5wgfgo9"
          }
        ]
      };
      break;
    case '/groups':
    case '/groups/groupId':
    case '/invites/join/link':
      data = {
        "errorDetail": "",
        "message": "",
        "response": [
          {
            "id": "vzUvMBGRaqoxY87XSTTou",
            "name": "Klasse 8c",
            "type": "school_class",
            "data": "A test school class",
            "invite": "vzUvOWSXAqjLvEFcWRT2e",
            "color": {"r": 15, "g": 254, "b": 54},
            "tags": [
              "vzUvMBNAYkJE2KlgJ5N5c",
              "vzUvMBNAd9FM6PAHW1MGm",
              "vzUvMBNAgWSSKXtF3EPqa"
            ],
            "owner": "vzUvMBytHtSTruVdpSYQy"
          }
        ]
      };
      break;
    case '/posts':
    case '/groups/groupId/posts':
    case '/groups/groupId/posts/postId':
      data = {
        "errorDetail": "",
        "message": "",
        "response": [
          {
            "id": "vzUvMBNGhCTl4jUyGXfk0",
            "user": "vzUvMBljOJDY2M1wENoLw",
            "group": "vzUvMBGRaqoxY87XSTTou",
            "title": "23.10.19 Mathe",
            "color": {"r": 125, "g": 255, "b": 12},
            "content": "Mathetest mit dem Thema rechnen",
            "completed": true,
            "creationTime": {
              "date": "2021-05-20 15:45:35.000000",
              "timezone_type": 3,
              "timezone": "Europe\/Berlin"
            },
            "expirationTime": {
              "date": "2100-11-11 10:52:32.000000",
              "timezone_type": 3,
              "timezone": "Europe\/Berlin"
            },
            "tags": ["vzUvMBNAd9FM6PAHW1MGm"]
          }
        ]
      };
      break;
    case '/groups/groupId/members/userId':
    case '/groups/groupId/members':
    case '/groups/groupId/members/self':
      data = {
        "errorDetail": "",
        "message": "",
        "response": [
          {
            "user": {
              "id": "vzUvMBytHtSTruVdpSYQy",
              "username": "TestUser3",
              "roles": ["ROLE_USER"],
              "posts": ["vzUvMBNMQKB6PqNwoKUBU", "vzUvMBNMU9glLGT9sRpz6"],
              "groups": ["vzUvMBGRaqoxY87XSTTou"]
            },
            "permissions": 15
          },
        ],
      };
      break;
    default:
      data = {
        'data':
            'PATH NOT REGISTERED! PLEASE CHANGE THE PATH OR REGISTER IT IN THE INTERCEPTOR!'
      };
  }
  options.data = data;
  return handler.resolve(Response(requestOptions: options, data: data), true);
}
