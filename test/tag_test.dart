import 'package:notingly_api/notingly_api.dart';
import 'package:test/test.dart';

import 'dio_interceptor.dart';

void main() async {
  final api = new NotinglyApi(
    getJWT: () => Future.value("jwt"),
    getJWTRefresh: () => Future.value('jwtRefresh'),
    getAPIUrl: () => Future.value('https://api.test'),
    setJWT: (_) => Future.value(),
    setJWTRefresh: (_) => Future.value(),
    enableLoggingInConsole: false,
  );
  api.httpClient.interceptors.add(createDioInterceptor());

  group('Tag', () {
    group('get tags by group', () {
      test('getting tags by group successfully', () async {
        List<Tag> tags = await api.getTags(groupId: Id('groupId'));
        expect(tags[0].id.value, equals('vzUvMBNAYkJE2KlgJ5N5c'));
        expect(tags[0].group.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(tags[0].name, equals('Important'));
        expect(tags[0].color.r, equals(125));
        expect(tags[0].color.g, equals(255));
        expect(tags[0].color.b, equals(12));
      });
      test('getting tags by not existing group', () async {
        expect(() async => await api.getTags(groupId: Id('notFound')),
            throwsA(isA<NotFoundException>()));
      });
    });
    group('get tag by id', () {
      test('get tag by id successfully', () async {
        Tag tag = await api.getTag(tagId: Id('tagId'), groupId: Id('groupId'));
        expect(tag.id.value, equals('vzUvMBNAYkJE2KlgJ5N5c'));
        expect(tag.group.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(tag.name, equals('Important'));
        expect(tag.color.r, equals(125));
        expect(tag.color.g, equals(255));
        expect(tag.color.b, equals(12));
      });
      test('get not existing tag', () async {
        expect(() async => await api.getTags(groupId: Id('notFound')),
            throwsA(isA<NotFoundException>()));
      });
    });
    group('create tag', () {
      test('create tag successfully', () async {
        // For simplicity reasons, the test api returns always the same tag
        Tag returnedTag = await api.createTag(
            name: "CreatedTag",
            color: ApiColor(10, 10, 10),
            groupId: Id("groupId"));
        expect(returnedTag.id.value, equals('vzUvMBNAYkJE2KlgJ5N5c'));
        expect(returnedTag.group.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(returnedTag.name, equals('Important'));
        expect(returnedTag.color.r, equals(125));
        expect(returnedTag.color.g, equals(255));
        expect(returnedTag.color.b, equals(12));
      });
      test('create tag with not existing group', () async {
        expect(
            () async => await api.createTag(
                  name: "CreatedTag",
                  groupId: Id('notFound'),
                  color: ApiColor(10, 10, 10),
                ),
            throwsA(isA<NotFoundException>()));
      });
    });
    group('update tag', () {
      test('update tag successfully', () async {
        // For simplicity reasons, the test api returns always the same tag
        Tag returnedTag = await api.updateTag(
            groupId: Id("groupId"),
            tagId: Id("tagId"),
            name: "UpdatedTag",
            color: ApiColor(10, 10, 10));
        expect(returnedTag.id.value, equals('vzUvMBNAYkJE2KlgJ5N5c'));
        expect(returnedTag.group.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(returnedTag.name, equals('Important'));
        expect(returnedTag.color.r, equals(125));
        expect(returnedTag.color.g, equals(255));
        expect(returnedTag.color.b, equals(12));
      });
      test('update tag with not existing group', () async {
        expect(
          () async => await api.updateTag(
            name: 'UpdatedTag',
            groupId: Id('notFound'),
            color: ApiColor(10, 10, 10),
            tagId: Id('notFound'),
          ),
          throwsA(isA<NotFoundException>()),
        );
      });
    });
    group('delete tag', () {
      test('delete tag successfully', () async {
        bool returned =
            await api.removeTag(tagId: Id('tagId'), groupId: Id('groupId'));
        expect(returned, isTrue);
      });
      test('update tag with not existing group', () async {
        expect(
          () async => await api.removeTag(
              tagId: Id('notFound'), groupId: Id('groupId')),
          throwsA(isA<NotFoundException>()),
        );
      });
    });
  });
}
