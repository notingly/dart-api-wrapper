import 'package:notingly_api/notingly_api.dart';
import 'package:test/test.dart';

import 'dio_interceptor.dart';

void main() async {
  final api = new NotinglyApi(
    getJWT: () => Future.value("jwt"),
    getJWTRefresh: () => Future.value('jwtRefresh'),
    getAPIUrl: () => Future.value('https://api.test'),
    setJWT: (_) => Future.value(),
    setJWTRefresh: (_) => Future.value(),
    enableLoggingInConsole: false,
  );
  api.httpClient.interceptors.add(createDioInterceptor());

  group('Media', () {
    group('get user avatar url', () {
      test('getting avatar url successfully', () async {
        final url = await api.getUserAvatarUrl(userId: Id('userId'));
        expect(url, 'https://api.test/media/avatars/userId');
      });
    });
  });
}
