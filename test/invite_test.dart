import 'package:logging/logging.dart';
import 'package:notingly_api/notingly_api.dart';
import 'package:test/test.dart';

import 'dio_interceptor.dart';

void main() async {
  final api = new NotinglyApi(
    getJWT: () => Future.value("jwt"),
    getJWTRefresh: () => Future.value('jwtRefresh'),
    getAPIUrl: () => Future.value('https://api.test'),
    setJWT: (_) => Future.value(),
    setJWTRefresh: (_) => Future.value(),
    enableLoggingInConsole: false,
  );
  api.httpClient.interceptors.add(createDioInterceptor());

  group('Invite', () {
    group('get invite by id', () {
      test('getting an invite successfully', () async {
        Invite invite = await api.getInviteById(id: Id('inviteId'));
        expect(invite.id.value, equals('vzD11k7FDURPr7ug7mosn'));
        expect(invite.group.value, equals('vzD11k79DGieFX4q4M3VV'));
        expect(invite.link, equals('A8j5wgfgo9'));
      });
      test('getting an invite that does not exist', () async {
        expect(() async => await api.getInviteById(id: Id('notFound')),
            throwsA(isA<NotFoundException>()));
      });
    });
    group('create invite', () {
      test('creating an invite successfully', () async {
        Invite invite = await api.createInvite(groupId: Id('inviteId'));
        expect(invite.id.value, equals('vzD11k7FDURPr7ug7mosn'));
        expect(invite.group.value, equals('vzD11k79DGieFX4q4M3VV'));
        expect(invite.link, equals('A8j5wgfgo9'));
      });
      test('creating an invite that does not exist', () async {
        expect(() async => await api.createInvite(groupId: Id('notFound')),
            throwsA(isA<NotFoundException>()));
      });
    });
    group('use invite', () {
      test('use invite successfully', () async {
        Group group = await api.useInviteLink(link: 'link');
        expect(group.id.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(group.name, equals('Klasse 8c'));
        expect(group.data, equals('A test school class'));
        expect(group.color.r, equals(15));
        expect(group.color.g, equals(254));
        expect(group.color.b, equals(54));
        expect(group.type, equals(GroupType.SCHOOL_CLASS));
      });
      test('use invite that does not exist', () async {
        expect(() async => await api.useInviteLink(link: 'notFound'),
            throwsA(isA<NotFoundException>()));
      });
    });
  });
}
