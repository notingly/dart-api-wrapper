import 'package:notingly_api/notingly_api.dart';
import 'package:test/test.dart';

import 'dio_interceptor.dart';

void main() async {
  final api = new NotinglyApi(
    getJWT: () => Future.value("jwt"),
    getJWTRefresh: () => Future.value('jwtRefresh'),
    getAPIUrl: () => Future.value('https://api.test'),
    setJWT: (_) => Future.value(),
    setJWTRefresh: (_) => Future.value(),
    enableLoggingInConsole: false,
  );
  api.httpClient.interceptors.add(createDioInterceptor());

  group('Group', () {
    group('get groups', () {
      test('get all groups a user is member of successfully', () async {
        List<Group> groups = await api.getGroups();
        expect(groups[0].id.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(groups[0].name, equals('Klasse 8c'));
        expect(groups[0].data, equals('A test school class'));
        expect(groups[0].type, equals(GroupType.SCHOOL_CLASS));
        expect(groups[0].invite.value, equals('vzUvOWSXAqjLvEFcWRT2e'));
        expect(groups[0].color.r, equals(15));
        expect(groups[0].color.g, equals(254));
        expect(groups[0].color.b, equals(54));
        expect(groups[0].owner.value, equals('vzUvMBytHtSTruVdpSYQy'));
      });
    });
    group('get a group by id', () {
      test('getting a group by id successfully', () async {
        Group group = await api.getGroup(id: Id('groupId'));
        expect(group.id.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(group.name, equals('Klasse 8c'));
        expect(group.data, equals('A test school class'));
        expect(group.type, equals(GroupType.SCHOOL_CLASS));
        expect(group.invite.value, equals('vzUvOWSXAqjLvEFcWRT2e'));
        expect(group.color.r, equals(15));
        expect(group.color.g, equals(254));
        expect(group.color.b, equals(54));
        expect(group.owner.value, equals('vzUvMBytHtSTruVdpSYQy'));
      });
      test('getting a group that does not exist', () async {
        expect(() async => await api.getGroup(id: Id('notFound')),
            throwsA(isA<NotFoundException>()));
      });
    });
    group('create a group', () {
      test('create successfully', () async {
        Group group = await api.createGroup(
          name: 'Group name',
          type: GroupType.SCHOOL_CLASS,
          description: 'Data',
          color: ApiColor(10, 20, 23),
        );
        // the group is always the same for testing sake. In real case,
        // the returned group would be the newly created one
        expect(group.id.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(group.name, equals('Klasse 8c'));
        expect(group.data, equals('A test school class'));
        expect(group.type, equals(GroupType.SCHOOL_CLASS));
        expect(group.invite.value, equals('vzUvOWSXAqjLvEFcWRT2e'));
        expect(group.color.r, equals(15));
        expect(group.color.g, equals(254));
        expect(group.color.b, equals(54));
        expect(group.owner.value, equals('vzUvMBytHtSTruVdpSYQy'));
      });
    });
    group('update a group', () {
      test('update a group successfully', () async {
        Group group = await api.updateGroup(
          id: Id('groupId'),
          name: 'Group name',
          description: 'Data',
          color: ApiColor(10, 20, 23),
        );
        // the group is always the same for testing sake. In real case,
        // the returned group would be the newly created one
        expect(group.id.value, equals('vzUvMBGRaqoxY87XSTTou'));
        expect(group.name, equals('Klasse 8c'));
        expect(group.data, equals('A test school class'));
        expect(group.type, equals(GroupType.SCHOOL_CLASS));
        expect(group.invite.value, equals('vzUvOWSXAqjLvEFcWRT2e'));
        expect(group.color.r, equals(15));
        expect(group.color.g, equals(254));
        expect(group.color.b, equals(54));
        expect(group.owner.value, equals('vzUvMBytHtSTruVdpSYQy'));
      });
      test('update not existing group', () {
        expect(
          () async => await api.updateGroup(
            id: Id('notFound'),
            name: 'Group name',
            description: 'Data',
            color: ApiColor(10, 20, 23),
          ),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('get all group members', () {
      test('get all group members successfully', () async {
        List<GroupMember> groupMembers =
            await api.getGroupMembers(groupId: Id('groupId'));
        expect(groupMembers[0].user.id.value, equals('vzUvMBytHtSTruVdpSYQy'));
        expect(groupMembers[0].permissions.toInt(), equals(15));
      });
      test('getting group members from group that does not exist', () {
        expect(
          () async => await api.getGroupMembers(groupId: Id('notFound')),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('get group member', () {
      test('get group member successfully', () async {
        GroupMember groupMember = await api.getGroupMember(
            groupId: Id('groupId'), userId: Id('userId'));
        expect(groupMember.user.id.value, equals('vzUvMBytHtSTruVdpSYQy'));
        expect(groupMember.permissions.toInt(), equals(15));
      });
      test('get group member from group that does not exist', () {
        expect(
          () async => await api.getGroupMember(
              groupId: Id('notFound'), userId: Id('notFound')),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('get self group member', () {
      test('get self group member successfully', () async {
        GroupMember groupMember =
            await api.getSelfGroupMember(groupId: Id('groupId'));
        expect(groupMember.user.id.value, equals('vzUvMBytHtSTruVdpSYQy'));
        expect(groupMember.permissions.toInt(), equals(15));
      });
      test('get self group member from group that does not exist', () {
        expect(
          () async => await api.getSelfGroupMember(groupId: Id('notFound')),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('add group member', () {
      test('add group member successfully', () async {
        bool boolean = await api.addGroupMember(
            groupId: Id('groupId'), userId: Id('userId'));
        expect(boolean, isTrue);
      });
      test('adding group member that does not exist', () {
        expect(
          () async {
            return await api.addGroupMember(
                groupId: Id('notFound'), userId: Id('userId'));
          },
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('update group member', () {
      test('update group member successfully', () async {
        bool groupMembers = await api.updateGroupMember(
          groupId: Id('groupId'),
          memberId: Id('userId'),
          permissions: GroupPermissions.empty(),
        );
        expect(groupMembers, isTrue);
      });
      test('update not existing group member', () {
        expect(
          () async => api.updateGroupMember(
            groupId: Id('groupId'),
            memberId: Id('notFound'),
            permissions: GroupPermissions.empty(),
          ),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('remove group member', () {
      test('remove group member successfully', () async {
        bool boolean = await api.removeGroupMember(
          groupId: Id('groupId'),
          userId: Id('userId'),
        );
        expect(boolean, isTrue);
      });
      test('remove not existing group member', () {
        expect(
          () async => await api.removeGroupMember(
            groupId: Id('groupId'),
            userId: Id('notFound'),
          ),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
    group('remove group', () {
      test('remove group successfully', () async {
        bool boolean = await api.removeGroup(
          id: Id('groupId'),
        );
        expect(boolean, isTrue);
      });
      test('remove not existing group', () {
        expect(
          () async => await api.removeGroup(
            id: Id('notFound'),
          ),
          throwsA(
            isA<NotFoundException>(),
          ),
        );
      });
    });
  });
}
